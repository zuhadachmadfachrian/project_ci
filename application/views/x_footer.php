   
    <footer class="templateux-footer bg-dark">
      <div class="container">

        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-3">
                <img src="<?php echo base_url().'assets/images/brand/logo.png'?>" alt="" width="70%">
              </div>
              <div class="col-3">
                <div class="block-footer-widget">
                  <h3>Support</h3>
                  <ul class="list-unstyled">
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Help Desk</a></li>
                    <li><a href="#">Knowledgebase</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-3">
                <div class="block-footer-widget">
                  <h3>About Us</h3>
                  <ul class="list-unstyled">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Careers</a></li>
                    <li><a href="#">Terms of Service</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                  </ul>
                </div>
              </div>

              <div class="col-3">
                <div class="block-footer-widget">
                  <p>Temukan Kami</p>
                  <ul class="list-unstyled block-social">
                    <li><a href="#" class="p-1"><span class="icon-facebook-square"></span></a></li>
                    <li><a href="#" class="p-1"><span class="icon-twitter"></span></a></li>
                    <li><a href="#" class="p-1"><span class="icon-github"></span></a></li>
                  </ul>
                </div>
              </div>
            </div> <!-- .row -->
          </div>
        </div>
        <div class="row pt-5 text-left">
          <div class="col-md-12 text-center text-white"><p>
            &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | by Shoesmart
          </p></div>
        </div>
      </div>
    </footer> 
   </div>   
  <script src="<?php echo base_url().'assets/js/scripts-all.js'?>"></script>
  <script src="<?php echo base_url().'assets/js/main.js'?>"></script>
  <script src="<?php echo base_url().'assets/jquery/jquery-2.2.3.min.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
  
  </body>
</html>