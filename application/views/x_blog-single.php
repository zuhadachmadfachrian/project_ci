<?php 
	$b=$data->row_array();
?>
    <div class="templateux-section" style="background-image: url(<?php echo base_url().'assets/images/aa.jpg'?>);">
      <div class="container">
        <div class="row align-items-center">
          
        </div>
      </div>
    </div> <!-- .templateux-cover -->

    <div id="blog" class="templateux-section">
    <div class="container">
    <iframe width="100%" height="70%" src="https://www.youtube.com/embed/UuLNCO4S4KY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      <!-- <img src="<?php echo base_url().'assets/images/'.$b['berita_image'];?>"> -->
            <div class="row mt-5">
              <div class="col-md-8">
                <h2 class="mb-3"><?php echo $b['berita_judul'];?></h2>

                <!-- <p><img src="images/img_7.jpg" alt="" class="img-fluid"></p> -->

                <?php echo $b['berita_isi'];?>

                <div class="tag-widget post-tag-container mb-5 mt-5">
                  <div class="tagcloud">
                    <a href="#" class="tag-cloud-link">Relax</a>
                    <a href="#" class="tag-cloud-link">Hotel</a>
                    <a href="#" class="tag-cloud-link">Luxury</a>
                    <a href="#" class="tag-cloud-link">Unwind</a>
                  </div>
                </div>


                <div class="pt-5 mt-5">
                  <h3 class="mb-5">6 Commentsaaa</h3>
                  <ul class="comment-list">
                    <li class="comment">
                      <div class="vcard bio">
                        <img src="<?php echo base_url().'assets/images/person_1.jpg'?>" alt="Image placeholder">
                      </div>
                      <div class="comment-body">
                        <h3>Jean Doe</h3>
                        <div class="meta">January 9, 2018 at 2:21pm</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
                        <p><a href="#" class="reply">Reply</a></p>
                      </div>
                    </li>

                    <li class="comment">
                      <div class="vcard bio">
                        <img src="<?php echo base_url().'assets/images/person_1.jpg'?>" alt="Image placeholder">
                      </div>
                      <div class="comment-body">
                        <h3>Jean Doe</h3>
                        <div class="meta">January 9, 2018 at 2:21pm</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
                        <p><a href="#" class="reply">Reply</a></p>
                      </div>

                      <ul class="children">
                        <li class="comment">
                          <div class="vcard bio">
                            <img src="<?php echo base_url().'assets/images/person_1.jpg'?>" alt="Image placeholder">
                          </div>
                          <div class="comment-body">
                            <h3>Jean Doe</h3>
                            <div class="meta">January 9, 2018 at 2:21pm</div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
                            <p><a href="#" class="reply">Reply</a></p>
                          </div>


                          <ul class="children">
                            <li class="comment">
                              <div class="vcard bio">
                                <img src="<?php echo base_url().'assets/images/person_1.jpg'?>" alt="Image placeholder">
                              </div>
                              <div class="comment-body">
                                <h3>Jean Doe</h3>
                                <div class="meta">January 9, 2018 at 2:21pm</div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
                                <p><a href="#" class="reply">Reply</a></p>
                              </div>

                                <ul class="children">
                                  <li class="comment">
                                    <div class="vcard bio">
                                      <img src="<?php echo base_url().'assets/images/person_1.jpg'?>" alt="Image placeholder">
                                    </div>
                                    <div class="comment-body">
                                      <h3>Jean Doe</h3>
                                      <div class="meta">January 9, 2018 at 2:21pm</div>
                                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
                                      <p><a href="#" class="reply">Reply</a></p>
                                    </div>
                                  </li>
                                </ul>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </li>

                    <li class="comment">
                      <div class="vcard bio">
                        <img src="<?php echo base_url().'assets/images/person_1.jpg'?>" alt="Image placeholder">
                      </div>
                      <div class="comment-body">
                        <h3>Jean Doe</h3>
                        <div class="meta">January 9, 2018 at 2:21pm</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
                        <p><a href="#" class="reply">Reply</a></p>
                      </div>
                    </li>
                  </ul>
                  <!-- END comment-list -->
                  
                  <div class="comment-form-wrap pt-5">
                    <h3 class="mb-5">Leave a comment</h3>
                    <form action="#" class="p-5 bg-light">
                      <div class="form-group">
                        <label for="name">Name *</label>
                        <input type="text" class="form-control" id="name">
                      </div>
                      <div class="form-group">
                        <label for="email">Email *</label>
                        <input type="email" class="form-control" id="email">
                      </div>
                      <div class="form-group">
                        <label for="message">Message</label>
                        <textarea name="" id="message" cols="30" rows="6" class="form-control"></textarea>
                      </div>
                      <div class="form-group">
                        <input type="submit" value="Post Comment" class="btn py-3 px-4 btn-primary">
                      </div>

                    </form>
                  </div>
                </div>

              </div> <!-- .col-md-8 -->
              <div class="col-md-4 sidebar">
                <div class="sidebar-box">
                  <form action="#" class="search-form">
                    <div class="form-group">
                      <span class="icon fa fa-search"></span>
                      <input type="text" class="form-control" placeholder="Type a keyword and hit enter">
                    </div>
                  </form>
                </div>
                <div class="sidebar-box">
                  <div class="categories">
                    <h3>Categories</h3>
                    <li><a href="#">Men <span>(12)</span></a></li>
                    <li><a href="#">Woman <span>(22)</span></a></li>
                    <li><a href="#">Girls <span>(37)</span></a></li>
                    <li><a href="#">Boys <span>(42)</span></a></li>
                  </div>
                </div>
                <div class="sidebar-box">
                  <img src="<?php echo base_url().'assets/images/sepatu.jpg'?>" alt="Image placeholder" class="img-fluid mb-4 rounded">
                  <h3>About The Author</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
                  <p><a href="#" class="btn btn-primary btn-lg">Read More</a></p>
                </div>

                <div class="sidebar-box">
                  <h3>Tag Cloud</h3>
                  <div class="tagcloud">
                    <a href="#" class="tag-cloud-link">Life</a>
                    <a href="#" class="tag-cloud-link">Sport</a>
                    <a href="#" class="tag-cloud-link">Tech</a>
                    <a href="#" class="tag-cloud-link">Travel</a>
                    <a href="#" class="tag-cloud-link">Life</a>
                    <a href="#" class="tag-cloud-link">Sport</a>
                    <a href="#" class="tag-cloud-link">Tech</a>
                    <a href="#" class="tag-cloud-link">Travel</a>
                  </div>
                </div>
              </div>

            </div>

            
          </div>
  </div>