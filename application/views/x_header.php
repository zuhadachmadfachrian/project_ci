<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Shoesmart</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/style.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
  </head>
  <body>
  
  <div class="js-animsition animsition" id="site-wrap" data-animsition-in-class="fade-in" data-animsition-out-class="fade-out">
    <header class="templateux-navbar dark" role="banner"> 
      <div class="container"  data-aos="fade-down">
        <div class="row">         
          <nav class="col-9 site-nav">
            <button class="d-block d-md-none hamburger hamburger--spin templateux-toggle templateux-toggle-light ml-auto templateux-toggle-menu" data-toggle="collapse" data-target="#mobile-menu" aria-controls="mobile-menu" aria-expanded="false" aria-label="Toggle navigation">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>

            <ul class="sf-menu templateux-menu d-none d-md-block">
              <li>
                <a href="index.php" class="animsition-link text-white">New Arrivals</a>
              </li>
              <li>
                <a href="#" class="animsition-link text-white">Shop</a>
                <ul>
                  <li><a href="#">Service 1</a></li>
                  <li><a href="#">Service 2</a></li>
                  <li>
                    <a href="#">Service 3</a>
                    <ul>
                      <li><a href="#">Service 1</a></li>
                      <li><a href="#">Service 2</a></li>
                      <li>
                        <a href="#">Service 3</a>
                        <ul>
                          <li><a href="#">Service 1</a></li>
                          <li><a href="#">Service 2</a></li>
                          <li><a href="#">Service 3</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li class="templateux-logo"><a href="<?php echo base_url().'index.php/post_berita'?>" class="animsition-link text-white">SHOESMART</a></li>
              <li><a href="<?php echo base_url().'index.php/post_berita/lists'?>" class="animsition-link text-white">Blog</a></li>
              <li><a href="contact.php" class="animsition-link text-white">Contact</a></li>
            </ul>
          </nav>
        </div> 
      </div> 
    </header> 